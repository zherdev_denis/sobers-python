import os


class Config(object):
    DEBUG = False
    TESTING = False

    BASEDIR = os.path.abspath(os.path.dirname(__file__))
    DATA_DIR = os.path.join(BASEDIR, 'data')
    OUTPUT_DIR = os.path.join(BASEDIR, 'output')
    TESTS_DIR = os.path.join(BASEDIR, 'tests')

    OUTPUT_FILE_NAME = 'output.csv'


class TestingConfig(Config):
    TESTING = True


class DevelopmentConfig(Config):
    DEBUG = True


class ProductionConfig(Config):
    pass
