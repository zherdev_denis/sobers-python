import os
import datetime
import csv

from decimal import Decimal
from dataclasses import dataclass, fields, astuple

from config import DevelopmentConfig


@dataclass
class AbstractRecord:
    timestamp: datetime
    type: str
    amount: Decimal
    id_from: int
    id_to: int
    source: str


class AbstractParser:

    def parse(self, record: dict):
        raise NotImplementedError


class Bank1Parser(AbstractParser):
    def parse(self, record: dict):
        return AbstractRecord(
            timestamp=datetime.datetime.strptime(record['timestamp'], '%b %d %Y').isoformat(),
            type=record['type'],
            amount=Decimal(record['amount']),
            id_from=int(record['from']),
            id_to=int(record['to']),
            source='bank1',
        )


class Bank2Parser(AbstractParser):
    def parse(self, record: dict):
        return AbstractRecord(
            timestamp=datetime.datetime.strptime(record['date'], '%d-%m-%Y').isoformat(),
            type=record['transaction'],
            amount=Decimal(record['amounts']),
            id_from=int(record['from']),
            id_to=int(record['to']),
            source='bank2',
        )


class Bank3Parser(AbstractParser):
    def parse(self, record: dict):
        return AbstractRecord(
            timestamp=datetime.datetime.strptime(record['date_readable'], '%d %b %Y').isoformat(),
            type=record['type'],
            amount=Decimal('%s.%s' % (record['euro'], record['cents'])),
            id_from=int(record['from']),
            id_to=int(record['to']),
            source='bank3',
        )


def parse_csv_file(name: str, parser: AbstractParser):
    result = []
    with open(name) as f:
        reader = csv.reader(f, delimiter=',')
        head = next(reader)
        for row in reader:
            result.append(parser.parse(record={k: v for (k, v) in zip(head, row)}))
    return result


def create_csv_file(path: str, files: tuple):
    head = [h.name for h in fields(AbstractRecord)]
    with open(path, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(head)
        for file in files:
            for row in file:
                writer.writerow(astuple(row))


if __name__ == '__main__':
    config = DevelopmentConfig()

    bank1_csv_file = parse_csv_file(os.path.join(config.BASEDIR, config.DATA_DIR, 'bank1.csv'), Bank1Parser())
    bank2_csv_file = parse_csv_file(os.path.join(config.BASEDIR, config.DATA_DIR, 'bank2.csv'), Bank2Parser())
    bank3_csv_file = parse_csv_file(os.path.join(config.BASEDIR, config.DATA_DIR, 'bank3.csv'), Bank3Parser())

    create_csv_file(os.path.join(config.BASEDIR, config.OUTPUT_DIR, config.OUTPUT_FILE_NAME), (bank1_csv_file, bank2_csv_file, bank3_csv_file))
